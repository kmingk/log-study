package com.yzm.log;

import com.alibaba.fastjson.JSON;
import org.apache.logging.log4j.core.util.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class TestMain {

    public static void main(String[] args) {

        // log4j的使用
        Logger logger = LoggerFactory.getLogger(TestMain.class);
        logger.trace("traceAA");
        logger.debug("debugAA");
        logger.info("infoAA");
        logger.warn("warnAA");
        logger.error("errorAA");


        // log4j整合slf4j
       // Logger logger = LoggerFactory.getLogger("mytest");


//        Log log = new Log();
//        log.setName("a");
//        log.setAge(11);
//        log.setDate(new Date());
//
//        logger.info(JSON.toJSONString(log));

      //  logger.error("{} ---- {}", "aa", "bbcc");



    }

}
